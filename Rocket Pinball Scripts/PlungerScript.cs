using UnityEngine;
using System.Collections;

public class PlungerScript : MonoBehaviour {
	
	GameObject gameManager;
	GameManager gameManagerScript;
	
	public Rigidbody pinball;
	public double coolDown;
	
	double plungerCooldown;
	
	AudioSource plungerAudio;

	// Use this for initialization
	void Awake () {
		gameManager = GameObject.FindGameObjectWithTag("GameManager");
		gameManagerScript = gameManager.GetComponent<GameManager>();
		
		plungerCooldown = Time.time + coolDown;
		
		plungerAudio = GetComponent(typeof(AudioSource)) as AudioSource;
	}
	
	// Update is called once per frame
	void Update () {
		
		if ( (Input.GetKey(KeyCode.Space)) && (Time.time >= plungerCooldown) && (gameManagerScript.GetPinballCount() > 0) && gameManagerScript.GetPlay () ) 
		{
			Rigidbody newPinball = (Rigidbody) Instantiate(pinball, this.transform.position, this.transform.rotation);
			newPinball.velocity = new Vector3(-25, 10, 0);			
			
			plungerCooldown = Time.time + coolDown;
			gameManagerScript.SetPinballLaunched();
			gameManagerScript.RemovePinball();
			
			plungerAudio.Play();
		}
	
	}
}

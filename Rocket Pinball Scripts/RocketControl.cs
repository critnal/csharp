using UnityEngine;
using System.Collections;

public class RocketControl : MonoBehaviour {
	
	public float rocketSpeedNormal;
	public float rocketSpeedPoweredUp;
	private float rocketSpeed;
	
	
	
	public Transform explosion;
	public Transform spawnPoint;
	
	GameObject launcher;
	
	GameObject gameManagerObject;
	GameManager gameManagerComponent;
	
	public LayerMask layerMask; //make sure we aren't in this layer 
    public float skinWidth = 0.1f; //probably doesn't need to be changed 
    
    private float minimumExtent; 
    private float partialExtent; 
    private float sqrMinimumExtent; 
    private Vector3 previousPosition; 
    private Rigidbody myRigidbody; 
	
	
	// Use this for initialization
	void Awake () {
		
		launcher = GameObject.FindGameObjectWithTag("Launcher");
		
		gameManagerObject = GameObject.FindGameObjectWithTag("GameManager");
		gameManagerComponent = gameManagerObject.GetComponent<GameManager>();
		
		if (gameManagerComponent.getPowerUp() == "speed")
			rocketSpeed = rocketSpeedPoweredUp;
		else
			rocketSpeed = rocketSpeedNormal;
		
		if (gameManagerComponent.getPowerUp() != "seeking")		
			this.rigidbody.AddForce(launcher.transform.forward * rocketSpeed, ForceMode.VelocityChange);
		
		myRigidbody = rigidbody; 
	    previousPosition = myRigidbody.position; 
	    minimumExtent = Mathf.Min(Mathf.Min(collider.bounds.extents.x, collider.bounds.extents.y), collider.bounds.extents.z); 
	    partialExtent = minimumExtent * (1.0f - skinWidth); 
	    sqrMinimumExtent = minimumExtent * minimumExtent; 
	
	}
	
	
    void FixedUpdate () {
		
		if (gameManagerComponent.getPowerUp() == "seeking")
		{
        	// Generate a plane that intersects the transform's position with an upwards normal.
   			Plane playerPlane = new Plane (Vector3.forward, transform.position);
		
        	// Generate a ray from the cursor position
        	Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
   
        	// Determine the point where the cursor ray intersects the plane.
        	// This will be the point that the object must look towards to be looking at the mouse.
        	// Raycasting to a Plane object only gives us a distance, so we'll have to take the distance,
        	//   then find the point along that ray that meets that distance.  This will be the point
        	//   to look at.
        	float hitdist = 0.0f;
        	// If the ray is parallel to the plane, Raycast will return false.
        	if (playerPlane.Raycast (ray, out hitdist))
        	{
           	 	// Get the point along the ray that hits the calculated distance.
           	 	Vector3 targetPoint = ray.GetPoint(hitdist);
       
            	// Determine the target rotation.  This is the rotation if the transform looks at the target point.
            	Quaternion targetRotation = Quaternion.LookRotation(targetPoint - transform.position);
       
            	// Smoothly rotate towards the target point.
            	transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.time);
				
				transform.Translate(Vector3.forward * Time.deltaTime * rocketSpeed);
        	}
		}
		
		//have we moved more than our minimum extent? 
       Vector3 movementThisStep = myRigidbody.position - previousPosition; 
       float movementSqrMagnitude = movementThisStep.sqrMagnitude;
        
       if (movementSqrMagnitude > sqrMinimumExtent) 
        { 
          float movementMagnitude = Mathf.Sqrt(movementSqrMagnitude);
          RaycastHit hitInfo; 
            
          //check for obstructions we might have missed 
          if (Physics.Raycast(previousPosition, movementThisStep, out hitInfo, movementMagnitude, layerMask.value)) 
             myRigidbody.position = hitInfo.point - (movementThisStep/movementMagnitude)*partialExtent; 
       } 
        
       previousPosition = myRigidbody.position; 
    }
	
	
	void OnCollisionEnter (Collision collidedObject) {
		
		if (gameManagerComponent.getPowerUp() != "damage")
		{
		
			if (collidedObject.transform.root.name == "Zone1"){
				gameManagerComponent.zone1Hit();
				gameManagerComponent.setLastHit();
			}
			else if (collidedObject.transform.root.name == "Zone2"){
				gameManagerComponent.zone2Hit();
				gameManagerComponent.setLastHit();
			}
			else if (collidedObject.transform.root.name == "Zone3"){
				gameManagerComponent.zone3Hit();
				gameManagerComponent.setLastHit();
			}
		}
		
		
		
		
		
		
		Explode ();
	}
	
	
	void Explode(){
		Instantiate (explosion, spawnPoint.position, new Quaternion(0, 0, 0, 0));
		DestroyObject (this.gameObject);
	}
}

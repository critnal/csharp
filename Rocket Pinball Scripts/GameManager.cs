using UnityEngine;
using System.Collections;




public class GameManager : MonoBehaviour {
	
	#region variables
	
	//game variables
	private int pinballCount;
	private int pinballsInPlay;
	
	public string powerup = "None";
	private double powerupTime;
	public double activePowerupTime;
	private int	 	powerupObject1,
					powerupObject2,
					powerupObject3;
	private int[] powerupArray = new int[] {0, 1, 2, 3, 4};
	
	private int currentScore = 0;
	private int[] highScore;
	
	private bool playGame = false;	
	private bool notFired = true;
	
	public int maxPinballCount;
	public bool useTapFire;
	
	//zone variables
	private int zone1HitCount = 0,
				zone2HitCount = 0,
				zone3HitCount = 0;	
	
	private bool zone1IsAlive = true,
				 zone2IsAlive = true,
				 zone3IsAlive = true;	
	
	private float zone1Timer = 0,
				  zone2Timer = 0,
				  zone3Timer = 0;
	
	public GameObject light1, light2, light3;
	private double light1Timer, light2Timer, light3Timer;
	
	public int maxZoneHitCount;
	private double lastHit = 0;
	public float zoneDownTime;
	
	
	#endregion
	
	// Use this for initialization
	void Start () {
		setPinballsToPlay();
		highScore = new int[10];		
	}
	
	
	// Update is called once per frame
	void FixedUpdate () {
		bool done = false;
		if ( ((pinballsInPlay == 0) && (pinballCount == 0) && !(notFired))
			  || ((!zone1IsAlive) && (!zone2IsAlive) && (!zone3IsAlive)) ){
			highScore = getHighScore ();
			for (int i = 0; i < 10; i++)
			{
				if (!done){
					if (currentScore > highScore[i])
					{
						highScore[i] = currentScore;
						done = true;
					}
				}
			}
			setHighScore (highScore);
			playGame = false;
			notFired = true;
			
			
		}
		
		
		if (zone1IsAlive)
		{			
			if (Time.time <= light1Timer)
				light1.light.enabled = true;
			
			if (zone1HitCount >= maxZoneHitCount)
			{
				zone1IsAlive = false;
				zone1Timer = Time.time + zoneDownTime;
			}
		} 
		else if (Time.time >= zone1Timer)
		{
			zone1IsAlive = true;
			zone1HitCount = 0;
		}
		
		
		if (zone2IsAlive)
		{			
			if (Time.time <= light2Timer)
				light2.light.enabled = true;
			
			if (zone2HitCount >= maxZoneHitCount)
			{
				zone2IsAlive = false;
				zone2Timer = Time.time + zoneDownTime;
			}
		} 
		else if (Time.time >= zone2Timer)
		{
			zone2IsAlive = true;
			zone2HitCount = 0;
		}
		
		
		if (zone3IsAlive)
		{			
			if (Time.time <= light3Timer)
				light3.light.enabled = true;
			
			if (zone3HitCount >= maxZoneHitCount)
			{
				zone3IsAlive = false;
				zone3Timer = Time.time + zoneDownTime;
			}
		} 
		else if (Time.time >= zone3Timer)
		{
			zone3IsAlive = true;
			zone3HitCount = 0;
		}
		
		if (Time.time > light1Timer)
			light1.light.enabled = false;
		
		if (Time.time > light2Timer)
			light2.light.enabled = false;
		
		if (Time.time > light3Timer)
			light3.light.enabled = false;
		
		doPowerUps();
	
	}
	
	
	#region pinball stuff
	
	public void SetPinballLaunched(){
		notFired = false;
	}
	
	public int GetPinballCount () {
		return pinballCount;		
	}	
	
	public void AddPinball () {
		pinballCount++;		
	}	
	
	public void RemovePinball () {
		pinballCount--;
		pinballsInPlay++;
	}
	
	
	public int getPinballCount (){
		return pinballCount;
	}
	
	public void setPinballsToPlay(){
		pinballCount = maxPinballCount;
	}
	
	public void RemovePinballFromPlay(){
		pinballsInPlay--;
	}
	
	public void spawnPinball () {
		pinballsInPlay++;
	}
	
	#endregion
	
	
	
	#region score stuff
	
	public void setHighScore(int[] myHighScore){
		for (int i = 0; i <10; i++)
		{
			PlayerPrefs.SetInt("highScore" + i,myHighScore[i]);
		}
	}
	
	public int[] getHighScore(){
		int[] tempArray = new int[10];
		for (int i = 0; i<10; i++)
		{
			tempArray[i] = PlayerPrefs.GetInt ("highScore" + i);
		}
		return tempArray;
	}
	public int getNextScore(){
		highScore = getHighScore ();
		for (int i = 0; i < 10; i++)
		{
			if (highScore[i]>currentScore)
				return highScore[i];
		}
		return currentScore;
	}
	public int getScore(){
		return currentScore;
	}
	public void setScore(int points){
		currentScore = currentScore + points;
	}
	
	public void resetScore(){
		currentScore = 0;
	}
	
		public void smallRolloverManager(string name, bool rBool){
		bool rolloverBool1 = false;
		bool rolloverBool2 = false;
		bool rolloverBool3 = false;
		
		
		if (name == "SmallRollover1")
			rolloverBool1 = rBool;
		else if (name == "SmallRollover2")
			rolloverBool2 = rBool;
		else
			rolloverBool3 = rBool;
		
		if (rolloverBool1 && rolloverBool2 && rolloverBool3)
			currentScore += 3000;
	}
	
	public bool bigRolloverManager(string name){
		bool rolloverBool1 = false;
		bool rolloverBool2 = false;
		bool rolloverBool3 = false;
		
		
		if (name == "BigRollover1")
			rolloverBool1 = true;
		else if (name == "BigRollover2")
			rolloverBool2 = true;
		else
			rolloverBool3 = true;
		
		if (rolloverBool1 && rolloverBool2 && rolloverBool3)
		{
			currentScore += 30000;
			rolloverBool1 = false;
			rolloverBool2 = false;
			rolloverBool3 = false;
			print("chaching");
			return true;
		}
		else
			return false;
	}
	
	#endregion
	


	#region zone stuff
	
	public bool getZoneIsAlive(string zone) {
		if (zone == "Zone1")
			return zone1IsAlive;
		else if (zone == "Zone2")
			return zone2IsAlive;
		else
			return zone3IsAlive;
	}
	
	
	
	public void zone1Hit () {
		if (Time.time >= lastHit){
			zone1HitCount++;
			light1Timer = Time.time + 0.3;
		}
	}
	
	public void zone2Hit () {
		if (Time.time >= lastHit){
			zone2HitCount++;
			light2Timer = Time.time + 0.3;
		}
	}
	
	public void zone3Hit () {
		if (Time.time >= lastHit){
			zone3HitCount++;
			light3Timer = Time.time + 0.3;
		}
	}
	
	public void setLastHit() {
		lastHit = Time.time + 0.1;
	}
	
	#endregion
	
	#region game stuff
	
	public bool GetPlay(){
		return playGame;
	}	
	
	public void SetPlay(bool myPlay){
		playGame = myPlay;
		
		if (myPlay == true){
			zone1HitCount = 0;
			zone2HitCount = 0;
			zone3HitCount = 0;
			
			zone1Timer = 0;
			zone2Timer = 0;
			zone3Timer = 0;
			
			zone1IsAlive = true;
			zone2IsAlive = true;
			zone3IsAlive = true;			
		}			
	}
	
	public bool getUseTapFire () {
		return useTapFire;
	}
	
	#endregion
	
	#region powerup stuff
	
	public string getPowerUp(){
		if (Time.time <= activePowerupTime)
			return powerup;
		else
			return "None";
	}
	
	public void setPowerUp (string newPowerup) {
		powerup = newPowerup;
		activePowerupTime = Time.time + 10;
	}
	
	public int objectPowerUpCheck (string name) {
		
		if (name == "PowerUp1")
			return powerupObject1;
		else if (name == "PowerUp2")
			return powerupObject2;
		else
			return powerupObject3;
	}
	
	void doPowerUps() {
		
		if (Time.time > powerupTime){
			
			for (int i = powerupArray.Length - 1; i > 0; i--) {
				int r = Random.Range(0, i);
				int tmp = powerupArray[i];
				powerupArray[i] = powerupArray[r];
				powerupArray[r] = tmp;				
			}
			
			powerupObject1 = powerupArray[0];
			powerupObject2 = powerupArray[1];
			powerupObject3 = powerupArray[2];			
			
			powerupTime = Time.time + 20;
		}		
	}
	
	
	
	
	#endregion

}

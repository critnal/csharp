﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Threading;
using System.Windows.Forms;

namespace SharedGamesClasses {
    public static class TwoUp {

        const string HEADS = "\n\tYou threw Heads - you win\n";
        const string ODDS = "\n\tYou threw Odds\n";
        const string TAILS = "\n\tYou threw Tails - I win\n";

        const string PLAY_AGAIN = "\n\nPlay again? (Y or N):";
        const string THROW_COINS = "\nPress any key to throw coins again.";

        private static Coin coin1;
        private static Coin coin2;

        private static int playerScore;
        private static int compScore;

        private static bool playAgain;





        /// <summary>
        /// Plays the Game of Two Up until the user decides to stop
        /// Used by Console version only
        /// </summary>
        public static void PlaySimple() {

            SetUpCoins();

            do { // while user has not exited

                do { // while there isn't a result

                    ThrowCoins();

                    if (thereIsNoWinner()) {
                        Console.WriteLine(ODDS);
                        WaitForKey_ConsoleOnly(THROW_COINS);
                    }

                } while (thereIsNoWinner());

                OutputWinner();

                OutputScores();

                playAgain = WantToPlayAgain_ConsoleOnly();

            } while (playAgain);

            // before exiting the game reset class variables
            // to appropriate starting values.
            ResetGlobals();

        } // end PlaySimple


        //-------------------------------------------------------------------------------------------------//


        /// <summary>
        /// Since class is static need to be able 
        /// to initialise local coins
        /// </summary>
        public static void SetUpCoins() {
            coin1 = new Coin();
            coin2 = new Coin();

        } // end SetUpCoins


        //-------------------------------------------------------------------------------------------------//

                
        /// <summary>
        /// Flip both coins
        /// </summary>
        public static void ThrowCoins() {
            coin1.Flip();
            coin2.Flip();

        } // end ThrowCoins


        //-------------------------------------------------------------------------------------------------//


        /// <summary>
        /// checks if both coins are equal
        /// </summary>
        /// <returns>
        /// true if coins are equal,
        /// false otherwise
        /// </returns>
        public static bool thereIsNoWinner() {
            return coin1.IsHeads() != coin2.IsHeads();

        } // end thereIsNoWinner


        //-------------------------------------------------------------------------------------------------//


        /// <summary>
        /// displays the winner
        /// </summary>
        public static void OutputWinner() {

            if (coin1.IsHeads() && coin2.IsHeads()) {
                playerScore++;
                Console.WriteLine(HEADS);

            } else if (!coin1.IsHeads() && !coin2.IsHeads()) {
                compScore++;
                Console.WriteLine(TAILS);
            }

        } // end OutputWinner


        //-------------------------------------------------------------------------------------------------//


        /// <summary>
        /// displays current score counts
        /// </summary>
        private static void OutputScores() {
            Console.WriteLine("\n\tYou have {0} points and I have {1} points.", playerScore, compScore);

        } // end OutputScores


        //-------------------------------------------------------------------------------------------------//


        /// <summary>
        /// Asks the user if they wish to play again.
        /// Pre:  none
        /// Post: whether player wishes to play again.
        /// </summary>
        /// <returns>
        /// true if player wishes to play again,
        /// false otherwise
        /// </returns>
        private static bool WantToPlayAgain_ConsoleOnly() {

            string userInput;

            do { // while input is not valid

                // prompt the player.
                Console.Write(PLAY_AGAIN);

                // get and check the player's input.
                userInput = Console.ReadLine();

                if (userInput != "Y" && userInput != "y" && userInput != "N" && userInput != "n") {
                    Console.WriteLine("\nPlease enter either Y or N");
                }

            } while (userInput != "Y" && userInput != "y" && userInput != "N" & userInput != "n");

            if (userInput == "Y" || userInput == "y") {
                return true;
            } else {
                return false;
            }

        } // end WantToPlayAgain_ConsoleOnly


        //-------------------------------------------------------------------------------------------------//


        /// <summary>
        /// Displays a prompt and waits for a key press.
        /// Pre:  prompt to display; not null, nor empty
        /// Post: prompt was displayed and user pressed a key.
        /// </summary>
        /// <param name="prompt">the prompt to display to the user</param>
        private static void WaitForKey_ConsoleOnly(string prompt) {
            Debug.Assert(!String.IsNullOrEmpty(prompt), "prompt != null");

            Console.WriteLine(prompt);
            Console.ReadKey();

        } // end WaitForKey_ConsoleOnly


        //-------------------------------------------------------------------------------------------------//


        /// <summary>
        /// Reset all class variables to "zeroed" values,
        /// counters and flags but not the coins.
        /// 
        /// used by both Console and GUI to ensure subsequent 
        /// playing of the game is correct
        /// </summary>
        public static void ResetGlobals() {
            playerScore = 0;
            compScore = 0;
            playAgain = true;

        } // end ResetGlobals

    } // end class TwoUp

} // end namespace


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharedGamesClasses {

    /// <summary>
    /// This class plays the game Ship, Captain and Crew.
    /// 
    /// There is a player and a computer. Each gets a single turn per game.
    /// The objective is to roll 5 dice and get a 6, a 5 and a 4; the two remaining dice
    /// are added together as a point total. The 6, 5 and 4 must be acquired in that order;
    /// and the final two dice are not totalled until then.
    /// 
    /// Finalised by 
    /// Alex Crichton
    /// n6878296
    /// 2011
    /// </summary>
    public static class Ship {

        const string PLYR_ROLLED = "\n\n\tYou have rolled ";
        const string PLYR_TURN = "\n\nPlayer's Turn";
        const string PLYR_HAS_SHIP = "\n\n\tYou have your Ship";
        const string PLYR_HAS_CAPTAIN = "\n\n\tYou have your Ship and Captain";
        const string PLYR_HAS_CREW = "\n\n\tYou have your Ship, Captain and Crew";
        const string PLYR_NO_CREW = "\n\n\tYou do not have you Ship, Captain and Crew";
        const string PLYR_HAS_POINTS = "\n\n\tYou have your Ship, Captain and Crew and your points total ";
        const string PLYR_HAS_WON = "\n\n\tYou have won";

        const string COMP_TURN = "\n\nComputer's Turn";
        const string COMP_ROLLED = "\n\n\tI have rolled ";
        const string COMP_HAS_SHIP = "\n\n\tI have my Ship";
        const string COMP_HAS_CAPTAIN = "\n\n\tI have my Ship and Captain";
        const string COMP_HAS_CREW = "\n\n\tI have my Ship, Captain and Crew";
        const string COMP_NO_CREW = "\n\n\tI do not have my Ship, Captain and Crew";
        const string COMP_HAS_POINTS = "\n\n\tI have my Ship, Captain and Crew and my points total ";
        const string COMP_HAS_WON = "\n\n\tI have won";

        const string DRAW = "\n\n\tThis game is a draw";

        // die values required for the 3 objectives
        const int SHIP = 6;     
        const int CAPTAIN = 5;  
        const int CREW = 4;

        const int NUM_FACES = 6;
        const int MAX_NUM_0F_DICE = 5;
        const int INITIAL_NUM_OF_TURNS = 0;
        const int FOUR = 4;
        const int THREE = 3;
        const int TWO = 2;
        const int MAX_NUM_OF_TURNS = 5;

        private static Die[] dice = new Die[5]; // 5 Dice 
        private static int[] face = new int[5]; // current face value of  5 dice
        private static bool[] whichDiceCanRoll = new bool[5]; // which dice can be rolled in the next round

        private static bool hasShip;
        private static bool hasCaptain;
        private static bool hasCrew;

        private static bool isPlayerTurn;
        private static bool isComputerTurn;

        private static int playerRollTotal;
        private static int computerRollTotal;
        private static int playerScore;
        private static int computerScore;
        private static int turnNum;
        private static int numDiceRemaining;





        /// <summary>
        /// Play the game Ship, Captain and Crew until the user decides to stop
        /// 
        /// pre:    none
        /// post:   user has finished playing Ship, Captain and Crew
        /// </summary>
        public static void PlayConsole() {

            do { // while user wants to play
                // play one game

                SetGlobals();

                PlayerTurn();

                SetGlobals();

                ComputerTurn();

                DisplayGameOutcome();

            } while (WantToPlayAgain_ConsoleOnly());

        } // end PlayConsole


        //-------------------------------------------------------------------------------------------------//


        /// <summary>
        /// set/reset global varibales.
        /// pre:    none
        /// post:   set global variables
        /// </summary
        private static void SetGlobals() {

            hasShip = false;
            hasCaptain = false;
            hasCrew = false;

            InitialiseShip();
            numDiceRemaining = MAX_NUM_0F_DICE;

            turnNum = INITIAL_NUM_OF_TURNS;
            isPlayerTurn = true;
            isComputerTurn = true;

        } // end SetGlobals


        //-------------------------------------------------------------------------------------------------//


        /// <summary>
        /// steps through the player's turn.
        /// player may roll up to 5 times to acquire their Ship, Captain and Crew
        /// and a desirable point total
        /// 
        /// pre:    none
        /// post:   player has had their turn
        /// </summary>
        private static void PlayerTurn() {

            Console.WriteLine(PLYR_TURN);
            WaitForKey_ConsoleOnly();

            do { // while still the player's turn

                DisplayTurnNum();
                DisplayDiceValues(PLYR_ROLLED);

                CheckIfHasShip();
                CheckIfHasCaptain();
                CheckIfHasCrew();

                UpdateNumRemainingDice();
                UpdateTotal(PLYR_TURN);
                UpdateTurn(PLYR_TURN);
                UpdateStatus(PLYR_TURN);

                if (isPlayerTurn) {
                    PlayerCanRollAgain();
                } else {
                    PlayerEndTurnMessage();
                }

            } while (isPlayerTurn);

        } // end PlayerTurn


        //-------------------------------------------------------------------------------------------------//


        /// <summary>
        /// steps through the computer's turn.
        /// computer may roll up to 5 times to acquire their Ship, Captain and Crew
        /// and a desirable point total.
        /// 
        /// pre:    none
        /// post:   computer has had their turn
        /// </summary>
        private static void ComputerTurn() {

            Console.WriteLine(COMP_TURN);
            WaitForKey_ConsoleOnly();

            do { // while still the computer's turn

                DisplayTurnNum();
                DisplayDiceValues(COMP_ROLLED);

                CheckIfHasShip();
                CheckIfHasCaptain();
                CheckIfHasCrew();

                UpdateNumRemainingDice();
                UpdateTotal(COMP_TURN);
                UpdateTurn(COMP_TURN);
                UpdateStatus(COMP_TURN);

                // if the computer has a higher roll total than the player
                // the computer's turn is ended
                // and thereby also the current game
                CheckIfCompHasWon();

                if (isComputerTurn) {
                    ComputerWillRollAgain();
                } else {
                    ComputerEndTurnMessage();
                }

            } while (isComputerTurn);

        } // end ComputerTurn


        //-------------------------------------------------------------------------------------------------//


        /// <summary>
        /// Initialise 5 dice 
        /// 
        /// pre:    dice, face and whichDiceCanRoll arrays initialised
        /// post:   creates an array of 5 dice with random face values and
        ///         and sets every bool in whichDiceCanRoll to true
        /// </summary>
        private static void InitialiseShip() {

            for (int i = 0; i < dice.Length; i++) {
                dice[i] = new Die(NUM_FACES);
                face[i] = dice[i].GetFaceValue();
                whichDiceCanRoll[i] = true;
            }

        } // end InitialiseShip


        //-------------------------------------------------------------------------------------------------//


        /// <summary>
        /// display the current turn of the current participant
        /// 
        /// pre:    none
        /// post:   display turn number in console
        /// </summary>
        private static void DisplayTurnNum() {
            Console.WriteLine("\n\nTurn " + (turnNum + 1));

        } // end DisplayTurnNum


        //-------------------------------------------------------------------------------------------------//


        /// <summary>
        /// Displays the face values of the dice rolled by who
        /// 
        /// pre:    dice and whichDiceCanRoll arrays initialised
        /// post:   display face values of every rollable dice in dice array
        /// </summary>
        /// <param name="who">PLAYER or COMPUTER</param>
        private static void DisplayDiceValues(string who) {
            string diceValues = who;

            for (int i = 0; i < dice.Length; i++) {
                if (whichDiceCanRoll[i]) {
                    diceValues += " " + face[i];
                }
            }
            Console.WriteLine(diceValues);

        } // end DisplayDiceValues


        //-------------------------------------------------------------------------------------------------//


        /// <summary>
        /// check if the current participant has acquired their Ship
        /// looks for first instance of face value equal to const int SHIP
        /// 
        /// pre:    face and whichDiceCanRoll arrays initialised
        /// 
        /// post:   returns true if Ship acquired and
        ///         sets relevant bool in whichDiceCanRoll to false
        ///         
        ///         returns false otherwise
        /// </summary>
        private static void CheckIfHasShip() {
            if (!hasShip) {

                // search for instance of face value equal to SHIP
                for (int i = 0; i < face.Length; i++) {
                    if (whichDiceCanRoll[i]) {
                        if (face[i] == SHIP) {

                            hasShip = true;
                            whichDiceCanRoll[i] = false;

                            // if found, don't look for another instance
                            return;
                        }
                    }
                }
            }

        } // end HasShip


        //-------------------------------------------------------------------------------------------------//


        /// <summary>
        /// check if the current participant has acquired their Captain
        /// looks for first instance of face value equal to const int CAPTAIN
        /// 
        /// pre:    face and whichDiceCanRoll arrays initialised
        /// 
        /// post:   returns true if Captain acquired and
        ///         sets relevant bool in whichDiceCanRoll to false
        ///         
        ///         returns false otherwise
        /// </summary>
        private static void CheckIfHasCaptain() {

            // only begin looking for a captain if there is already a ship
            if (hasShip && !hasCaptain) {

                // search for instance of face value equal to CAPTAIN
                for (int i = 0; i < face.Length; i++) {
                    if (whichDiceCanRoll[i]) {
                        if (face[i] == CAPTAIN) {
                            
                            hasCaptain = true;
                            whichDiceCanRoll[i] = false;

                            // if found, don't look for another instance
                            return;
                        }
                    }
                }
            }

        } // end HasCaptain


        //-------------------------------------------------------------------------------------------------//


        /// <summary>
        /// check if the current participant has acquired their Crew
        /// looks for first instance of face value equal to const int CREW
        /// 
        /// pre:    face and whichDiceCanRoll arrays initialised
        /// 
        /// post:   returns true if Crew acquired and
        ///         sets relevant bool in whichDiceCanRoll to false
        ///         
        ///         returns false otherwise
        /// </summary>
        private static void CheckIfHasCrew() {

            // only begin looking for crew if there is already a ship and captain
            if (hasCaptain && !hasCrew) {

                // search for instance of face value equal to CREW
                for (int i = 0; i < face.Length; i++) {
                    if (whichDiceCanRoll[i]) {
                        if (face[i] == CREW) {

                            hasCrew = true;
                            whichDiceCanRoll[i] = false;

                            // if found, don't look for another instance
                            return;
                        }
                    }
                }
            }

        } // end HasCrew


        //-------------------------------------------------------------------------------------------------//


        /// <summary>
        /// check which dice are still available to be rolled
        /// 
        /// pre:    whichDiceCanRoll array initialised
        /// post:   sets global variable numDiceRemaining
        ///         to number of remaining dice
        /// </summary>
        private static void UpdateNumRemainingDice() {

            int count = 5;

            // decrement count for every bool in whichDiceCanRoll that
            // is set to false
            foreach (bool canRoll in whichDiceCanRoll) {
                if (!canRoll) {
                    count--;
                }
            }

            numDiceRemaining = count;

        } // end DisplayRemainingDice


        //-------------------------------------------------------------------------------------------------//


        /// <summary>
        /// calculate the total of the remaining two dice.
        /// only when participant has acquired their Ship, Captain and Crew
        /// 
        /// pre:    participant has Ship, Captain and Crew
        /// post:   set total to appropriate global participant total
        /// </summary>
        /// <param name="who">PLAYER or COMPUTER</param>
        private static void UpdateTotal(string who) {            

            // no need to calculate total if there is no
            // ship, captain and crew
            if (hasCrew) {

                int total = 0;

                // only add face values of dice that 
                // can be rolled
                for (int i = 0; i < dice.Length; i++) {
                    if (whichDiceCanRoll[i]) {
                        total += face[i];
                    }
                }

                // set total to appropriate global participant total
                if (who == PLYR_TURN) {
                    playerRollTotal = total;
                } else {
                    computerRollTotal = total;
                }
            }            

        } // end UpdateTotal


        //-------------------------------------------------------------------------------------------------//


        /// <summary>
        /// update the current participant's turn number and
        /// turn status
        /// 
        /// pre:    none
        /// post:   increment turn by 1
        /// 
        ///         if turn number greater than max allowed turns
        ///         set turn to false
        /// </summary>
        /// <param name="who">PLAYER or COMPUTER</param>
        private static void UpdateTurn(string who) {

            turnNum++;

            if (who == PLYR_TURN) {
                if (turnNum >= MAX_NUM_OF_TURNS) {
                    isPlayerTurn = false;
                }

            } else {
                if (turnNum >= MAX_NUM_OF_TURNS) {
                    isComputerTurn = false;
                }
            }

        } // end UpdateTurn


        //-------------------------------------------------------------------------------------------------//


        /// <summary>
        /// displays if the current participant has their Ship, Captain and/or Crew
        /// and their current roll total
        /// 
        /// pre:    none
        /// post:   displays current participant's Ship status, and
        ///         if applicable, their roll total
        /// </summary>
        /// <param name="who">PLAYER or COMPUTER</param>
        private static void UpdateStatus(string who) {

            // base message to be displayed to user
            string status = "";

            if (who == PLYR_TURN) {

                if (hasShip) {
                    status = PLYR_HAS_SHIP;

                    if (hasCaptain) {
                        status = PLYR_HAS_CAPTAIN;

                        if (hasCrew) {
                            status = PLYR_HAS_CREW + " and your points total " + playerRollTotal;
                        }
                    }
                }

            } else {

                if (hasShip) {
                    status = COMP_HAS_SHIP;

                    if (hasCaptain) {
                        status = COMP_HAS_CAPTAIN;

                        if (hasCrew) {
                            status = COMP_HAS_CREW + " and my points total " + computerRollTotal;
                        }
                    }
                }
            }

            Console.WriteLine(status);

        } // end UpdateShipStatus


        //-------------------------------------------------------------------------------------------------//


        /// <summary>
        /// checks if the computer has acquired a greater
        /// roll total than the player
        /// 
        /// pre:    none
        /// post:   sets isComputerTurn to false if computerRollTotal > playerRollTotal
        /// </summary>
        private static void CheckIfCompHasWon() {
            if (computerRollTotal > playerRollTotal) {
                isComputerTurn = false;
            }

        } // end CheckIfCompHasWon


        //-------------------------------------------------------------------------------------------------//


        /// <summary>
        /// displays a message at the end of the player's turn
        /// if necessary
        /// 
        /// pre:    none
        /// post:   if player doesn't have their Crew
        ///         displays message notifying them
        /// </summary>
        private static void PlayerEndTurnMessage() {

            if (!hasCrew) {
                Console.WriteLine(PLYR_NO_CREW);
            }

        } // end DisplayEndTurnMessage


        //-------------------------------------------------------------------------------------------------//


        /// <summary>
        /// displays a message at the end of the computer's turn
        /// if necessary
        /// 
        /// pre:    none
        /// post:   if computer doesn't have their Crew
        ///         displays message notifying user
        /// </summary>
        private static void ComputerEndTurnMessage() {

            if (!hasCrew) {
                Console.WriteLine(COMP_NO_CREW);
            }

        } // end ComputerEndTurnMessage


        //-------------------------------------------------------------------------------------------------//


        /// <summary>
        /// if the player can roll again, ask them if
        /// they would like to. otherwise, end the 
        /// player's turn
        /// 
        /// pre:    none
        /// post:   either continue or end player's turn
        /// </summary>
        private static void PlayerCanRollAgain() {

            // if player does NOT have ship, captain and crew
            // they are simply prompted to roll again
            if (!hasCrew) {
                Console.WriteLine("\n\nPress enter to roll {0} dice again", numDiceRemaining);
                WaitForKey_ConsoleOnly();
                RerollDice();

            // if they DO have ship, captain and crew
            // ask if they want to roll their dice again
            // for chance at better total
            } else if (PlyrWantToReroll_ConsoleOnly()) {
                RerollDice();

            // if player doesn't want to roll again
            // end player's turn
            } else {
                isPlayerTurn = false;
            }

        } // end PlayerCanRollAgain


        //-------------------------------------------------------------------------------------------------//


        /// <summary>
        /// prompt user to continue computer's turn
        /// 
        /// pre:    none
        /// post:   continue computer's turn
        /// </summary>
        private static void ComputerWillRollAgain() {

            Console.WriteLine("\n\nPress enter to roll {0} dice again", numDiceRemaining);
            WaitForKey_ConsoleOnly();
            RerollDice();

        } // end ComputerWillRollAgain


        //-------------------------------------------------------------------------------------------------//


        /// <summary>
        /// rolls every dice that can be rolled
        /// 
        /// pre:    dice, face and whichDiceCanRoll arrays initialised
        /// post:   roll every dice that can be rolled
        /// </summary>
        private static void RerollDice() {

            // every dice that can be rolled
            // is rolled again
            for (int i = 0; i < dice.Length; i++) {
                if (whichDiceCanRoll[i]) {
                    face[i] = dice[i].Roll();
                }
            }

        } // end RollAgain


        //-------------------------------------------------------------------------------------------------//


        /// <summary>
        /// displays winner of the current game and
        /// displays current player and computer scores
        /// 
        /// pre:    none
        /// post:   display winner and participant scores
        /// </summary>
        private static void DisplayGameOutcome() {

            if (playerRollTotal > computerRollTotal) {
                Console.WriteLine(PLYR_HAS_WON);
                playerScore++;

            } else if (playerRollTotal < computerRollTotal) {
                Console.WriteLine(COMP_HAS_WON);
                computerScore++;

            } else {
                Console.WriteLine(DRAW);
            }

            Console.WriteLine("\n\nYou have won {0} games and I have won {1} games.", 
                playerScore, computerScore);

        } // end DisplayGameOutcome


        //-------------------------------------------------------------------------------------------------//


        /// <summary>
        /// Waits for a key press.
        /// 
        /// pre:    none
        /// post:   continues when key pressed
        /// </summary>
        private static void WaitForKey_ConsoleOnly() {
            Console.ReadKey();

        } // end WaitForKey_ConsoleOnly


        //-------------------------------------------------------------------------------------------------//


        /// <summary>
        /// ask player if they want to roll again
        /// 
        /// pre:    none
        /// post:   whether player wants to roll again
        /// </summary>
        /// <returns>
        /// true if Y or y
        /// false if N or n</returns>
        private static bool PlyrWantToReroll_ConsoleOnly() {

            string userInput;

            do { // while input is not valid

                // prompt the player.
                Console.WriteLine("\n\nWould you like to roll your {0} dice again? (Y or N):", numDiceRemaining);

                // get and check the player's input.
                userInput = Console.ReadLine();

                if (userInput != "Y" && userInput != "y" && userInput != "N" && userInput != "n") {
                    Console.WriteLine("**Please enter either Y or N");
                }

            } while (userInput != "Y" && userInput != "y" && userInput != "N" & userInput != "n");

            if (userInput == "Y" || userInput == "y") {
                return true;
            } else {
                return false;
            }

        } // end WantToPlayAgain_ConsoleOnly


        //-------------------------------------------------------------------------------------------------//


        /// <summary>
        /// Asks the user if they wish to play again.
        /// Pre:  none
        /// Post: whether player wishes to play again.
        /// </summary>
        /// <returns>
        /// true if player wishes to play again,
        /// false otherwise
        /// </returns>
        private static bool WantToPlayAgain_ConsoleOnly() {

            string userInput;

            do { // while input is not valid

                // prompt the player.
                Console.WriteLine("\n\n\nPlay again? (Y or N):");

                // get and check the player's input.
                userInput = Console.ReadLine();

                if (userInput != "Y" && userInput != "y" && userInput != "N" && userInput != "n") {
                    Console.WriteLine("**Please enter either Y or N");
                }

            } while (userInput != "Y" && userInput != "y" && userInput != "N" & userInput != "n");

            if (userInput == "Y" || userInput == "y") {
                return true;
            } else {
                return false;
            }

        } // end WantToPlayAgain_ConsoleOnly


    }// end class Ship
} // end namespace


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Diagnostics;
using System.Windows.Forms;

namespace SharedGamesClasses {

    /// <summary>
    /// This class plays the game Snake Eyes
    /// 
    /// There is a player and a computer. Only the player rolls.
    /// 
    /// Finalised by
    /// Alex Crichton
    /// n6878296
    /// 2011
    /// </summary>
    public static class SnakeEyes {
       
        const int SNAKE_EYES = 2;
        const int SEVEN = 7;
        const int ELEVEN = 11;
        const int THREE = 3;
        const int TWELVE = 12;
        const int TWO = 2;

        const string WIN = "\n\tCongratulations you win!";
        const string LOSE = "\n\tSorry you lose";
        
        static int rollTotal = 0; // total of the two die 
        static int rollAgainTotal = 0; // total of subsequent rolls
        static int playerScore = 0;
        static int compScore = 0;
        static int winner;

        static bool needToRollAgain = false;

        static Die die1 = new Die();
        static Die die2 = new Die();

        enum Winner {player, comp}





        /// <summary>
        /// Plays the Game of Snake Eyes until the user decides to stop
        /// 
        /// pre:    none
        /// post:   plays Snake Eyes until player stops
        /// </summary>
        public static void PlayConsole() {

            do { // until the user wants to stop 

                PlayFirstRoll();
                Console.WriteLine("\n\tYour dice roll totalled {0}", rollTotal);
                
                if (needToRollAgain) {
                    SecondRoll_ConsoleOnly();                
                }

                OutputWinners();

            } while (WantToPlayAgain_ConsoleOnly());

        } //end Play_Console


        //-------------------------------------------------------------------------------------------------//

        
        /// <summary>
        /// Rolls the two Dice and 
        /// calls FirstRoll 
        /// No additional code is required in this method
        /// </summary>
        private static void PlayFirstRoll() {
            RollDice();
            FirstRoll();

        }// end PlayFirstRoll


        //-------------------------------------------------------------------------------------------------//


        /// <summary>
        /// rolls the two die and
        /// set rollTotal 
        /// </summary>
        private static void RollDice() {  
            rollTotal = die1.Roll() + die2.Roll();
  
        } //end RollDice


        //-------------------------------------------------------------------------------------------------//


        /// <summary>
        ///  Determines the outcome of the first roll, 
        ///  setting appropriate class variables 
        ///  depending upon dice total.
        /// </summary>
        private static void FirstRoll() {
            
            if (rollTotal == SNAKE_EYES) {
                playerScore += TWO;
                Console.WriteLine("\n\tSnake Eyes!");
                winner = (int) Winner.player;

            } else if (rollTotal == SEVEN || rollTotal == ELEVEN) {
                playerScore++;
                winner = (int) Winner.player;

            } else if (rollTotal == THREE || rollTotal == TWELVE) {
                compScore += TWO;
                winner = (int) Winner.comp;

            } else {
                needToRollAgain = true;
            }

        } // end FirstRoll


        //-------------------------------------------------------------------------------------------------//


        /// <summary>
        /// Repeatedly calls RollAgain until a result is obtained. 
        /// </summary>
        private static void SecondRoll_ConsoleOnly() {
           
            do { // while needToRollAgain

                Console.WriteLine("\nPress any key to roll dice again to obtain " +
                                    "your {0} points.", rollTotal);
                WaitForKey_ConsoleOnly();
                                
                RollAgain();

                Console.WriteLine("\n\tYour dice roll totalled {0}.", rollAgainTotal);

            } while (needToRollAgain);
           
        } //end SecondRollConsoleOnly


        //-------------------------------------------------------------------------------------------------//
        

        /// <summary>
        /// Rolls the dice on a subsequent roll and
        /// sets appropriate class variables
        /// </summary>
        private static void RollAgain() {

            rollAgainTotal = die1.Roll() + die2.Roll();

            if (rollAgainTotal == rollTotal) {
                playerScore += rollTotal;
                winner = (int) Winner.player;
                needToRollAgain = false;

            } else if (rollAgainTotal == SEVEN) {
                compScore += TWO;
                winner = (int) Winner.comp;
                needToRollAgain = false;
            }

        }//end RollAgain


        //-------------------------------------------------------------------------------------------------//

        
        /// <summary>
        /// displays current game winner 
        /// and overall score for player and computer
        /// </summary>
        public static void OutputWinners() {

            if (winner == (int)Winner.player) {
                Console.WriteLine(WIN);

            } else if (winner == (int)Winner.comp) {
                Console.WriteLine(LOSE);            
            }

            Console.WriteLine("\n\tYou have {0} points and I have {1} points.", playerScore, compScore);

        } //end OutputWinners


        //-------------------------------------------------------------------------------------------------//


        /// Asks the user if they wish to play again.
        /// Pre:  none
        /// Post: whether player wishes to play again.
        /// </summary>
        /// <returns>
        /// true if player wishes to play again,
        /// false otherwise
        /// </returns>
        private static bool WantToPlayAgain_ConsoleOnly() {

            string userInput;

            do { // while input is not valid

                // prompt the player.
                Console.WriteLine("\nPlay again? (Y or N):");

                // get and check the player's input.
                userInput = Console.ReadLine();

                if (userInput != "Y" && userInput != "y" && userInput != "N" && userInput != "n") {
                    Console.WriteLine("\nPlease enter either Y or N");
                }

            } while (userInput != "Y" && userInput != "y" && userInput != "N" & userInput != "n");

            if (userInput == "Y" || userInput == "y") {
                return true;
            } else {
                return false;
            }

        } // end WantToPlayAgain_ConsoleOnly


        //-------------------------------------------------------------------------------------------------//


        /// <summary>
        /// Waits for a key press.
        /// </summary>
        private static void WaitForKey_ConsoleOnly() {
            Console.ReadKey();

        } // end WaitForKey_ConsoleOnly


        //-------------------------------------------------------------------------------------------------//
        

        /// <summary>
        /// Entry point for GUI to play Snake Eyes
        /// 
        /// No additional code is required in this method
        /// </summary>
        /// <returns> true if dice have to be rolled again,
        ///           otherwise false , indicates a result
        /// </returns>
        public static bool PlayGUI() {
            
            PlayFirstRoll();
            return true; //********** inserted to allow code to compile *************************

        } //end Play_GUI


        //-------------------------------------------------------------------------------------------------//


        /// <summary>
        /// Subsequent roll of dice by GUI as required.
        /// 
        /// No additional code is required in this method
        /// </summary>
        /// <returns> 
        /// true if dice have to be rolled again,
        /// otherwise false , indicates a result
        /// </returns>
        public static bool SecondRollGUI() {

            RollAgain();
            return true; //********** inserted to allow code to compile *************************


        } //end SecondRollGUI
        
    }//end class SnakeEyes
}//end namespace


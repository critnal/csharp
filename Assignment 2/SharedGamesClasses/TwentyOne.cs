﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Diagnostics;


namespace SharedGamesClasses {
    public static class TwentyOne {

        const int PLAYER = 0;
        const int DEALER = 1;

        const int ACE_ELEVEN = 11;
        const int ACE_ONE = 1;
        const int TEN = 10;
        const int TWO = 2;
        const int TWENTY_ONE = 21;

        const string PLAYERS_NAME = "Player";
        const string DEALERS_NAME = "Dealer";

        private static Pack pack = new Pack();
        private static ArrayList[] hands = new ArrayList[2];
    
        private static bool isPlayersTurn;
        private static bool isDealersTurn;

        private static int playerHandTotal = 0;
        private static int dealerHandTotal = 0;
        private static int playerScore = 0;
        private static int dealerScore = 0;

        private static string gameWinner;
        private static string outputMessage;






        /// <summary>
        /// Plays the game of Twenty One until the user decides to stop.
        /// </summary>
        public static void Play() {            

            do { // until user chooses to stop

                SetUpGame();

                PlayerTurn();

                if (isDealersTurn) {
                    DealerTurn();
                }
            
                OutputWinner();

                DisplayScores();
                            
            } while (WantToPlayAgain_ConsoleOnly());

        } // end Play


        //-------------------------------------------------------------------------------------------------//


        /// <summary>
        /// Set up to play the game, 
        /// by shuffling the pack of cards and 
        /// dealing the two hands 
        /// </summary>
        private static void SetUpGame() {
            for (int i = 0; i < hands.Length; i++) {
                hands[i] = new ArrayList();
            }

            DealHands();

            DisplayHand(hands[PLAYER], PLAYERS_NAME);
            DisplayHand(hands[DEALER], DEALERS_NAME);

        } // end SetUpGame


        //-------------------------------------------------------------------------------------------------//


        /// <summary>
        /// deals both hands.
        /// first shuffle entire deck
        /// then deal two cards each
        /// </summary>
        private static void DealHands() {
            pack.Shuffle();
            hands[PLAYER] = pack.DealTwoCards();
            hands[DEALER] = pack.DealTwoCards();

        } // end DealHands


        //-------------------------------------------------------------------------------------------------//


        /// <summary>
        /// steps through the player's turn.
        /// check their hand for 21 and bust,
        /// then ask if they want to hit
        /// </summary>
        private static void PlayerTurn() {            

            do { // until player stays

                playerHandTotal = CurrentPoints(PLAYER);

                if (playerHandTotal == TWENTY_ONE) {
                    gameWinner = PLAYERS_NAME;
                    outputMessage = "player 21";
                    isPlayersTurn = false;
                    isDealersTurn = false;

                } else if (isBust(playerHandTotal)) {
                    gameWinner = DEALERS_NAME;
                    outputMessage = "player bust";
                    isPlayersTurn = false;
                    isDealersTurn = false;

                } else if (PlayerWantToHit()) {
                    Hit(PLAYER);
                    DisplayHand(hands[PLAYER], PLAYERS_NAME);
                    isPlayersTurn = true;

                } else {
                    isPlayersTurn = false;
                    isDealersTurn = true;
                    Console.WriteLine("\n\tPlayer stays with {0} points.", playerHandTotal);
                }               

            } while (isPlayersTurn);

        } // end PlayerTurn


        //-------------------------------------------------------------------------------------------------//


        /// <summary>
        /// step through dealer's turn.
        /// if dealer hasn't bust or got a higher card total
        /// than the player they must roll again
        /// </summary>
        private static void DealerTurn() {

            do { // until dealer wins or loses

                dealerHandTotal = CurrentPoints(DEALER);

                if (isBust(dealerHandTotal)) {
                    outputMessage = "dealer bust";
                    gameWinner = PLAYERS_NAME;
                    isDealersTurn = false;

                } else if (dealerHandTotal > playerHandTotal) {
                    gameWinner = DEALERS_NAME;
                    isDealersTurn = false;

                } else if (dealerHandTotal <= playerHandTotal) {
                    Hit(DEALER);
                    Console.WriteLine("\n\tDealer Hit");
                    DisplayHand(hands[DEALER], DEALERS_NAME);
                    isDealersTurn = true;
                }                

            } while (isDealersTurn);

        } // end DealerTurn


        //-------------------------------------------------------------------------------------------------//


        /// <summary>
        /// calculate value of current participant's hand.
        /// </summary>
        /// <param name="who">PLAYER or DEALER</param>
        /// <returns>hand total of current participant</returns>
        private static int CurrentPoints(int who) {

            ArrayList hand = hands[who];
            Card card; 
            int handTotal = 0;            

            // add every card in the current hand together
            for (int i = 0; i < hand.Count; i++ ) {

                card = (Card) hand[i];

                if (card.GetFaceValue() == FaceValue.Ace) {

                    // an ace can be worth 1 or 11.
                    // if hand worth less than 11 ace is worth 11
                    // if hand worth more than 11 ace is worth 1
                    if (handTotal < ACE_ELEVEN) {
                        handTotal += ACE_ELEVEN;
                    } else {
                        handTotal += ACE_ONE;
                    }

                } else if (((int)card.GetFaceValue() + TWO) > TEN) {
                    handTotal += TEN;

                } else {
                    handTotal += (int)card.GetFaceValue() + TWO;
                }
            }

            return handTotal;

        } //end Play


        //-------------------------------------------------------------------------------------------------//


        /// <summary>
        /// checks if the current hand has gone bust
        /// </summary>
        /// <param name="handTotal">current participant's hand value</param>
        /// <returns>
        /// true if hand is bust
        /// false otherwise
        /// </returns>
        private static bool isBust(int handTotal) {
            return handTotal > TWENTY_ONE;

        } // end Bust


        //-------------------------------------------------------------------------------------------------//


        /// <summary>
        /// ask the player if they want to hit
        /// </summary>
        /// <returns>
        /// true if player wants to hit
        /// false otherwise</returns>
        private static bool PlayerWantToHit() {

            string userInput;

            do { // while input is not valid

                // prompt the player.
                Console.WriteLine("\nDo you want to hit (H) or stay (S):");

                // get the player's input.
                userInput = Console.ReadLine();

                if (userInput != "H" && userInput != "h" && userInput != "S" && userInput != "s") {
                    Console.WriteLine("\nPlease enter either Y or N");
                }

            } while (userInput != "H" && userInput != "h" && userInput != "S" & userInput != "s");

            if (userInput == "H" || userInput == "h") {
                return true;
            } else {
                return false;
            }

        } // end PlayerWantToHit


        //-------------------------------------------------------------------------------------------------//


        /// <summary>
        /// check if the dealer wants to hit
        /// </summary>
        /// <returns>
        /// true if dealer hand less than player hand
        /// false otherwise
        /// </returns>
        private static bool DealerWantToHit() {
            return dealerHandTotal <= playerHandTotal;

        } // end DealerWantToHit


        //-------------------------------------------------------------------------------------------------//


        /// <summary>
        /// participant hits.
        /// deals one additional card to their hand.
        /// </summary>
        /// <param name="who">the participant whose hand it is</param>
        private static void Hit(int who) {
            hands[who].Add(pack.DealOneCard());

        } // end Hit


        //-------------------------------------------------------------------------------------------------//

            
        /// <summary>
        /// Output the hand of cards of either the Player or the Dealer
        /// </summary>
        /// <param name="hand">a player's hand which consists of two or more cards</param>
        /// <param name="who">the player whose hand it is</param>
        public static void DisplayHand(ArrayList hand, string who) {

            // the items of the hand arraylist parameter need to be assigned
            // to individual card objects in order to perform the GetFaceValue()
            // accessor method on them
            Card tempCard1 = (Card) hand[0];
            Card tempCard2 = (Card) hand[1];

            Console.Write("\n\t{0} has {1}, {2}", who, tempCard1.GetFaceValue(), tempCard2.GetFaceValue());

            if (hand.Count > 2) {

                for (int i = 2; i < hand.Count; i++) {

                    Card tempCard = (Card)hand[i];
                    Console.Write(", {0}", tempCard.GetFaceValue());
                }
            }

            Console.WriteLine();
            
        } // end DisplayHand


        //-------------------------------------------------------------------------------------------------//


        /// <summary>
        /// display winner of the current game
        /// </summary>
        private static void OutputWinner() {

            switch (outputMessage) {
 
                case "player 21":
                    Console.WriteLine("\n\tPlayer wins with 21!");
                    break;

                case "player bust":
                    Console.WriteLine("\n\tPlayer has gone bust with {0} points", playerHandTotal);
                    break;

                case "dealer bust" :
                    Console.WriteLine("\n\tDealer has gone bust with {0} points", dealerHandTotal);
                    break;

                default :
                    Console.WriteLine("\n\tPlayer has {0} points, Dealer has {1} points.", playerHandTotal, dealerHandTotal);
                    break;
            }
            
            Console.WriteLine("\n\t{0} wins", gameWinner);

            outputMessage = "null";
           
        } // end OutPutWInner


        //-------------------------------------------------------------------------------------------------//


        /// <summary>
        /// display current participant scores
        /// </summary>
        private static void DisplayScores() {

            UpdateScores();

            Console.WriteLine("\nPlayer has won {0} games and Dealer has won {1} games.", 
                playerScore, dealerScore);


        } // end DisplayScores


        //-------------------------------------------------------------------------------------------------//


        /// <summary>
        /// add one to the winner's score
        /// </summary>
        private static void UpdateScores() {
            if (gameWinner == "Player") {
                playerScore++;

            } else {
                dealerScore++;
            }

        } // end UpdateScores


        //-------------------------------------------------------------------------------------------------//

        
        /// Asks the user if they wish to play again.
        /// Pre:  none
        /// Post: whether player wishes to play again.
        /// </summary>
        /// <returns>
        /// true if player wishes to play again,
        /// false otherwise
        /// </returns>
        private static bool WantToPlayAgain_ConsoleOnly() {

            string userInput;

            do { // while input is not valid

                // prompt the player.
                Console.WriteLine("\nPlay again? (Y or N):");

                // get and check the player's input.
                userInput = Console.ReadLine();

                if (userInput != "Y" && userInput != "y" && userInput != "N" && userInput != "n") {
                    Console.WriteLine("**Please enter either Y or N");
                }

            } while (userInput != "Y" && userInput != "y" && userInput != "N" & userInput != "n");

            if (userInput == "Y" || userInput == "y") {
                return true;
            } else {
                return false;
            }

        } // end WantToPlayAgain_ConsoleOnly
        
    } // end class TwentyOne
} // end namespace

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;


using SharedGamesClasses;

namespace ConsoleGames {
    /// <summary>
    /// Console interface code which displays an initial menu
    /// from which a user selects an option and then the appropriate sub-menu
    /// is displayed and the user selects a game to be played.
    /// 
    /// Author: Mike Roggenkamp
    /// 
    /// Version: September 2011
    /// 
    /// Based on numerous Console Menu Driven programs in various languages by the same author since 1981.
    /// 
    /// Modification:  Used the Trace class rather than Console class to demonstrate using 
    /// a Trace Listener to redirect output. Though the output is still Console.Out
    /// 
    /// No modifications to be made to this class
    /// 
    /// </summary>
    class ConsoleMain {
        const int EXIT = 0;

        const int CARD = 1;
        const int COIN = 2;
        const int DICE = 3;

        const int TWENTY_ONE = 1;

        const int SIMPLE_TWO_UP = 1;

        const int SNAKE_EYES = 1;
        const int SHIP_CAPTAIN_CREW = 2;

        const int NOT_YET_AVAILABLE = 2;

        const int NOT_YET_AVAIL = 3;

        static int menuOption = EXIT;
       
        static string mainMenu = "\n\n\tWelcome to Games Online \n\n" +
                                   "To make a selection: Enter the number of" +
                                   " your selection.\n\n" +
                                   "\t 1. Card Games.\n\n" +
                                   "\t 2. Coin Games\n\n" +
                                   "\t 3. Dice Games.\n\n\n" +
                                   "\t 0. Exit the program.\n\n";
      
        static string cardMenu = "\n\t 1. Twenty-One" +
                                 "\n\t 2. Not yet available." +
                                  "\n\n\t 0. Cancel Selection:";

        static string diceMenu = "\n\t 1. Snake Eyes." +
                                 "\n\t 2. Ship, Captain & Crew." +
                                 "\n\t 3. Not yet available." +
                                 "\n\n\t 0. Cancel Selection:";

        static string CoinMenu = "\n\t 1. Two-Up\n" +
                                 "\n\t 2. Not yet available." +
                                 "\n\n\t 0. Cancel Selection";


        static void Main(string[] args) {
            
            Trace.Listeners.Add(new TextWriterTraceListener(Console.Out));
            Trace.AutoFlush = true;

            do {
                DisplayMenu(mainMenu);
                menuOption = GetMenuOption(EXIT, DICE);
                switch (menuOption) {
                    case CARD:
                        DisplayMenu(cardMenu);
                        menuOption = GetMenuOption(EXIT, TWENTY_ONE);
                        PlayCardGame(menuOption);
                        break;

                    case COIN:
                        DisplayMenu(CoinMenu);
                        menuOption = GetMenuOption(EXIT, SIMPLE_TWO_UP);
                        PlayCoinGame(menuOption);
                        break;

                    case DICE:
                        DisplayMenu(diceMenu);
                        menuOption = GetMenuOption(EXIT, SHIP_CAPTAIN_CREW);
                        PlayDiceGame(menuOption);
                        break;

                    case EXIT:
                        DisplayEndMessage();
                        break;

                    default:
                        Trace.WriteLine("Major Logical error - this message should not appear!");
                        break;

                }// end switch
            } while (menuOption != EXIT);
        } //end Main

        public static void DisplayMenu(string menu) {
            Trace.WriteLine(menu);
            Trace.Write("\n What game do you wish to play? ");
        }// end DisplayMenu

        static void PressAny() {
            Trace.Write("\nPress any key to terminate program ...");
            Console.ReadLine();
        }// end PressAny

        static void DisplayEndMessage() {
            Trace.WriteLine("\n\tThank you for using Games Online\n\n");
            PressAny();
        }//end DisplayEndMessage

        public static int GetMenuOption(int lower, int upper) {
            int option;
            do {
                option = int.Parse(Console.ReadLine());
                if ((option < lower) || (option > upper)) {
                    Trace.WriteLine(String.Format("\n\nPlease enter a number between"
                                      + " {0} and {1}\n", lower, upper));
                }
            } while ((option < lower) || (option > upper));
            return option;
        }//end GetMenuOption


        static void PlayCardGame(int menuOption) {
            switch (menuOption) {
                case TWENTY_ONE:
                    TwentyOne.Play();
                    break;

                default:
                    break;

            }
        } //end PlaycardGame

        static void PlayCoinGame(int menuOption) {
            switch (menuOption) {
                case SIMPLE_TWO_UP:
                    TwoUp.PlaySimple();
                    break;

               default:
                    break;

            }

        }// end PlayCoinGame

        static void PlayDiceGame(int menuOption) {
            switch (menuOption) {
                case SNAKE_EYES:
                    SnakeEyes.PlayConsole();
                    break;

                case SHIP_CAPTAIN_CREW:
                    Ship.PlayConsole();
                    break;

                default:
                  
                    break;

            }
        } //end PlayDiceGame 

        
    }//end class consoleMain
}//end namespace
